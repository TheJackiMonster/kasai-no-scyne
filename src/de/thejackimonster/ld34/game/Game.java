package de.thejackimonster.ld34.game;

import java.awt.Container;

import javax.swing.JApplet;
import javax.swing.JFrame;

import de.thejackimonster.ld34.entity.player.Action;
import de.thejackimonster.ld34.entity.player.Player;
import de.thejackimonster.ld34.entity.player.Scyne;
import de.thejackimonster.ld34.gfx.Screen;
import de.thejackimonster.ld34.gfx.Sprite;
import de.thejackimonster.ld34.gfx.View;
import de.thejackimonster.ld34.sfx.Sound;

public class Game extends JApplet {

	private static final long serialVersionUID = 5575829828613436894L;

	public void init() {}

	public void start() {
		Game.start(this);
	}

	public void stop() {}

	public void destroy() {}

	public static void main(String[] args) {
		final JFrame frame = new JFrame();
		
		frame.setSize(800, 600);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Game.start(frame);
	}

	private static final void start(Container container) {
		final GameControls controls = new GameControls();
		
		final Screen screen = new Screen(container, controls);
		final View view = screen.createView();
		
		container.setVisible(true);
		
		new Thread() {
		
			public void run() {
				Player player = new Scyne();
				GameState state = null;
				
				Sound.RUN.play();
				
				long currentTime = System.currentTimeMillis();
				long lastTime = currentTime;
				float deltaTime, lastValue = 0;
				float startsInTime = -1F;
				
				while (true) {
					currentTime = System.currentTimeMillis();
					deltaTime = 0.001F * Math.abs(currentTime - lastTime);
					
					if (state != null) {
						final float value = ((state.getSpeed() - Player.SPEED) / Player.MAX_SPEED);
						final boolean dead = !state.update(value, deltaTime, controls);
						
						state.render(value, lastValue, screen, view);
						
						lastTime = currentTime;
						
						if (dead) {
							player.health--;
							
							if (player.health <= 0) {
								player = new Scyne();
								state = null;
							} else {
								player.reset();
								state = new GameState(player);
							}
							
							lastValue = 0;
						} else {
							lastValue = value;
						}
					} else {
						if ((controls.onAction(Action.DASH)) || (controls.onAction(Action.JUMP))) {
							startsInTime = 3000;
						}
						
						if (screen.render()) {
							screen.drawSprite(Sprite.START, startsInTime > 0? 1 : 0, view.fillRect());
						}
						
						if (startsInTime > 0) {
							startsInTime -= deltaTime;
							
							if (startsInTime <= 0) {
								state = new GameState(player);
								startsInTime = -1F;
							}
						}
					}
					
					try {
						Thread.sleep(1L);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		
		}.start();
	}

}
