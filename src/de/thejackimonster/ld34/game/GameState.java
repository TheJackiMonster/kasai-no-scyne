package de.thejackimonster.ld34.game;

import java.util.ArrayList;

import de.thejackimonster.ld34.entity.Rock;
import de.thejackimonster.ld34.entity.Street;
import de.thejackimonster.ld34.entity.StreetBuilder;
import de.thejackimonster.ld34.entity.event.Event;
import de.thejackimonster.ld34.entity.particle.FireParticle;
import de.thejackimonster.ld34.entity.player.Action;
import de.thejackimonster.ld34.entity.player.Mode;
import de.thejackimonster.ld34.entity.player.Player;
import de.thejackimonster.ld34.gfx.Screen;
import de.thejackimonster.ld34.gfx.Sprite;
import de.thejackimonster.ld34.gfx.View;
import de.thejackimonster.ld34.world.World;

public final class GameState {

	private final World world;
	private final Player player;
	private final StreetBuilder builder;
	private final ArrayList<Event> events;

	protected GameState(Player current) {
		world = new World();
		player = current;
		builder = new StreetBuilder();
		events = new ArrayList<Event>();
		
		//world.add(player);
		
		for (int i = 0; i < Street.COUNT; i++) {
			world.add(builder.createElement(world, Street.SIZE));
		}
	}

	public final float getSpeed() {
		return player.speed;
	}

	protected final boolean update(float value, float deltaTime, GameControls controls) {
		if (controls.onAction(Action.JUMP)) {
			player.action(Action.JUMP);
		}
		
		if (controls.onAction(Action.DASH)) {
			player.action(Action.DASH);
		}
		
		world.update(deltaTime);
		player.update(deltaTime);
		
		player.top = world.topAt(events, player);
		
		if (events.size() > 0) {
			for (int i = events.size() - 1; i >= 0; i--) {
				events.get(i).dispose(world, player);
				events.remove(i);
			}
			
			player.top = world.topAt(null, player);
		}
		
		if (world.streetCount() < Street.COUNT) {
			world.add(builder.createElement(world, Street.SIZE));
			//player.points++;
		} else
		if (Math.random() < 0.001F) {
			world.add(new Rock(player));
		}
		
		for (int i = 0; i < player.motion[0] / player.speed; i++) {
			world.add(new FireParticle(player));
		}
		
		player.speed += deltaTime * (Player.MAX_SPEED - value) * 0.005F;
		
		return !player.removed;
	}

	protected final boolean render(float value, float lastValue, Screen screen, View view) {
		view.lookAt(player.bounds);
		
		view.position[0] += (0.5F - (value + lastValue) / 2) * 0.8F * view.size[0];
		view.position[1] -= 0.1F * view.size[1];
		
		if (screen.render()) {
			final float[] rect = view.fillRect();
			
			screen.drawSprite(Sprite.BACKGROUND, 0, rect);
			
			rect[1] = -0.01F;
			rect[3] -= rect[1];
			
			rect[0] = 1 - (System.currentTimeMillis() % 10000) * 0.0001F;
			
			screen.drawSprite(Sprite.BACKGROUND, 1, rect);
			
			rect[0] -= 1;
			
			screen.drawSprite(Sprite.BACKGROUND, 1, rect);
			
			world.render(screen, view);
			player.render(screen, view);
			
			rect[0] = 0.03F;
			rect[1] = 0.01F;
			
			
			screen.drawText(String.valueOf(player.points), rect);
			
			rect[0] = 1.0F - player.health * 0.06F;
			rect[2] = 0.05F;
			rect[3] = 0.05F;
			
			if (player.health > 0) {
				for (int i = 0; i < player.health; i++) {
					screen.drawSprite(Sprite.HEALTH, 0, rect);
					rect[0] += 0.06F;
				}
			}
			
			rect[0] = 0.01F;
			rect[1] = 0.07F;
			
			screen.drawSprite(Sprite.FIRE, 0, rect);
			
			if (player.mode == Mode.FIRE) {
				screen.drawSprite(Sprite.FIRE, 1, rect);
			}
			
			return true;
		} else {
			return false;
		}
	}

}
