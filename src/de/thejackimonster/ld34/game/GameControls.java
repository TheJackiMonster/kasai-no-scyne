package de.thejackimonster.ld34.game;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import de.thejackimonster.ld34.entity.player.Action;

public final class GameControls implements KeyListener {

	private final int[] actions;
	public final int[] actionKeys;

	public GameControls() {
		actions = new int[Action.values().length];
		actionKeys = new int[actions.length];
		
		actionKeys[0] = KeyEvent.VK_SPACE;
		actionKeys[1] = KeyEvent.VK_X;
	}

	public final boolean onAction(Action action) {
		if (actions[action.ordinal()] > 0) {
			actions[action.ordinal()] *= -1;
			return true;
		} else {
			return false;
		}
	}

	public final void keyTyped(KeyEvent e) {}

	public final void keyPressed(KeyEvent e) {
		for (int i = 0; i < actionKeys.length; i++) {
			if ((e.getKeyCode() == actionKeys[i]) && (actions[i] == 0)) {
				actions[i] = 1;
			}
		}
	}

	public final void keyReleased(KeyEvent e) {
		for (int i = 0; i < actionKeys.length; i++) {
			if ((e.getKeyCode() == actionKeys[i]) && (actions[i] < 0)) {
				actions[i] = 0;
			}
		}
	}

}
