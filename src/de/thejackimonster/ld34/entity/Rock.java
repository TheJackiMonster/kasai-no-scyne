package de.thejackimonster.ld34.entity;

import java.util.List;

import de.thejackimonster.ld34.entity.event.Event;
import de.thejackimonster.ld34.entity.event.RockEvent;
import de.thejackimonster.ld34.entity.player.Player;
import de.thejackimonster.ld34.gfx.Screen;
import de.thejackimonster.ld34.gfx.Sprite;
import de.thejackimonster.ld34.gfx.View;

public final class Rock extends Entity {

	private final float limit;

	private float action;

	public Rock(Player player) {
		super();
		
		position[0] = (float) (player.position[0] + player.speed * 4);
		position[1] = (float) (player.position[1] - player.speed * 4);
		
		size[0] *= 1.5F;
		size[1] *= 1.5F;
		
		limit = player.position[1] + player.size[1] * 3;
		action = (int) (Math.random() * 4);
		
		motion[0] = -Player.SPEED / player.speed;
		motion[1] = 10;
	}

	public void update(float time) {
		action += time * 5;
		
		motion[1] = Math.min(motion[1] + 9.81F * time, 10);
		
		final float x = position[0] + motion[0] * time;
		final float y = position[1] + motion[1] * time;
		
		position[0] = x;
		position[1] = y;
		
		if (y >= limit) {
			removed = true;
		}
	}

	public void render(Screen screen, View view) {
		screen.drawSprite(Sprite.ROCK, (int) (action % 4), view.transformRect(bounds));
	}

	public float beginTop() {
		return Float.NaN;
	}

	public float topAt(List<Event> events, float[][] bounds) {
		if ((bounds[0][0] < position[0] + size[0]) && (bounds[0][0] + bounds[1][0] >= position[0])) {
			if ((events != null) && (bounds[0][1] < position[1] + size[1])) {
				events.add(new RockEvent(this));
			}
		}
		
		return Float.NaN;
	}

}
