package de.thejackimonster.ld34.entity;

import de.thejackimonster.ld34.world.World;

public final class StreetBuilder {

	private Street last;

	public StreetBuilder() {
		last = null;
	}

	public final Street createElement(World world, int length) {
		final boolean hole = ((last != null) && (!last.isHole) && (Math.random() > 0.9F));
		
		final Street element = new Street(length, hole);
		
		if (last != null) {
			element.position[0] = last.position[0] + last.size[0];
			element.position[1] += last.endTop() - element.beginTop();
		}
		
		if ((!element.isHole) && (Math.random() > 0.6F)) {
			final Wall wall = new Wall(1 + (int) (Math.random() * 3));
			
			wall.position[0] = (float) (element.position[0] + (element.size[0] - wall.size[0]) * Math.random());
			wall.position[1] = element.topAt(null, wall.bounds) - wall.size[1] * 0.9F;
			
			world.add(wall);
		}
		
		last = element;
		return element;
	}

}
