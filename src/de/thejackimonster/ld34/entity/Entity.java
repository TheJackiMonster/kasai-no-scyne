package de.thejackimonster.ld34.entity;

import java.util.List;

import de.thejackimonster.ld34.entity.event.Event;
import de.thejackimonster.ld34.gfx.Screen;
import de.thejackimonster.ld34.gfx.View;

public abstract class Entity {

	public final float[] position;
	public final float[] size;
	public final float[] motion;

	public final float[][] bounds;

	public boolean removed;

	public Entity() {
		position = new float[] { 0, 0 };
		size = new float[] { 1, 1 };
		motion = new float[] { 0, 0 };
		
		bounds = new float[][] {
			position, size, motion
		};
		
		removed = false;
	}

	public void update(float time) {
		position[0] += motion[0] * time;
		position[1] += motion[1] * time;
	}

	public abstract void render(Screen screen, View view);

	public float beginTop() {
		return position[1];
	}

	public float topAt(List<Event> events, float[][] bounds) {
		if ((bounds[0][0] < position[0] + size[0]) && (bounds[0][0] + bounds[1][0] >= position[0])) {
			return beginTop();
		} else {
			return Float.NaN;
		}
	}

	public float endTop() {
		return beginTop();
	}

}
