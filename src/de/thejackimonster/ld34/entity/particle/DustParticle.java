package de.thejackimonster.ld34.entity.particle;

import de.thejackimonster.ld34.entity.Rock;
import de.thejackimonster.ld34.entity.Wall;
import de.thejackimonster.ld34.gfx.Screen;
import de.thejackimonster.ld34.gfx.Sprite;
import de.thejackimonster.ld34.gfx.View;

public final class DustParticle extends Particle {

	public DustParticle(Wall wall, float[] v) {
		super(wall);
		
		position[0] = wall.position[0] + wall.size[0] / 2;
		position[1] = (float) (wall.position[1] + wall.size[1] * Math.random());
		
		size[0] *= 0.5F;
		size[1] *= 0.5F;
		
		motion[0] = (float) (Math.random() * 2 - 1) * 5 + v[0] / 2;
		motion[1] = (float) (Math.random() * 2 - 1) * 5 + v[0] / 2;
	}

	public DustParticle(Rock rock, float[] v) {
		super(rock);
		
		position[0] = (float) (rock.position[0] + rock.size[0] * Math.random());
		position[1] = (float) (rock.position[1] + rock.size[1] * Math.random());
		
		motion[0] = (float) (Math.random() * 2 - 1) * 5 + v[0] / 2;
		motion[1] = (float) (Math.random() * 2 - 1) * 5 + v[0] / 2;
	}

	public void render(Screen screen, View view) {
		screen.drawSprite(Sprite.PARTICLE, id + 4, view.transformRect(bounds));
	}

}
