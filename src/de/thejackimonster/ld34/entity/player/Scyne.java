package de.thejackimonster.ld34.entity.player;

import de.thejackimonster.ld34.gfx.Screen;
import de.thejackimonster.ld34.gfx.Sprite;
import de.thejackimonster.ld34.gfx.View;

public final class Scyne extends Player {

	private float id;

	public Scyne() {
		super(Scyne.class.getSimpleName());
		offset = 0;
		id = 0;
	}

	public final void update(float time) {
		super.update(time);
		
		if (mode == Mode.GROUND) {
			id = (id + time * 10) % 6;
		} else
		if (mode == Mode.FIRE) {
			id = (id + time * 20) % 6;
		}
	}

	public final void render(Screen screen, View view) {
		screen.drawSprite(Sprite.SCYNE, (int) id, view.transformRect(bounds));
	}

	public final void action(Action action) {
		switch (action) {
		case JUMP:
			if (mode == Mode.GROUND) {
				motion[1] = Math.max(motion[1] - 16, -10);
				mode = Mode.JUMP;
			} else
			if (mode == Mode.JUMP) {
				motion[1] = Math.max(motion[1] - 6, -10);
				mode = Mode.AIR;
			} else
			if (mode == Mode.FIRE) {
				motion[1] = Math.max(motion[1] - 1, -10);
			}
			
			break;
		case DASH:
			if (mode == Mode.FIRE) {
				final float delta = (motion[0] - speed);
				
				motion[1] = Math.max(Math.max(-delta, -speed), -10);
				motion[0] = speed / 2;
				
				mode = Mode.ROCKET;
			} else
			if (mode != Mode.ROCKET) {
				motion[0] = speed * 2.5F;
				mode = Mode.FIRE;
			}
			
			break;
		}
	}

}
