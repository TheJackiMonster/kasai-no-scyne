package de.thejackimonster.ld34.entity.event;

import de.thejackimonster.ld34.entity.player.Player;
import de.thejackimonster.ld34.world.World;

public abstract class Event {

	public abstract void dispose(World world, Player player);

}
