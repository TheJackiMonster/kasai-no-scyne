package de.thejackimonster.ld34.entity.event;

import de.thejackimonster.ld34.entity.Rock;
import de.thejackimonster.ld34.entity.particle.DustParticle;
import de.thejackimonster.ld34.entity.player.Mode;
import de.thejackimonster.ld34.entity.player.Player;
import de.thejackimonster.ld34.world.World;

public final class RockEvent extends Event {

	private final Rock parent;

	public RockEvent(Rock rock) {
		super();
		parent = rock;
	}

	public void dispose(World world, Player player) {
		if (player.mode != Mode.FIRE) {
			player.motion[0] *= 0.3F;
			player.motion[1] *= 0.3F;
			
			player.speed -= player.speed * 0.05F;
			//player.health--;
			player.crashes++;
		} else
		if (player.speed < Player.MAX_SPEED) {
			player.speed += (Player.MAX_SPEED - player.speed) * 0.005F;
			player.points++;
		}
		
		for (int i = 0; i < (int) (1 + parent.size[1] * 10); i++) {
			world.add(new DustParticle(parent, player.motion));
		}
		
		parent.removed = true;
	}

}
