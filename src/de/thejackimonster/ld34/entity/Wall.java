package de.thejackimonster.ld34.entity;

import java.util.List;

import de.thejackimonster.ld34.entity.event.Event;
import de.thejackimonster.ld34.entity.event.WallEvent;
import de.thejackimonster.ld34.gfx.Screen;
import de.thejackimonster.ld34.gfx.Sprite;
import de.thejackimonster.ld34.gfx.View;

public final class Wall extends Entity {

	public boolean passed;

	public Wall(int elements) {
		super();
		
		position[0] = 7;
		
		motion[0] = 0;
		motion[1] = 0;
		
		size[0] = 1;
		size[1] = elements;
		
		passed = false;
	}

	public void render(Screen screen, View view) {
		final int elements = (int) (size[1] / size[0]);
		
		final float[][] wall = { { position[0], position[1] }, { size[0], size[0] } };
		
		for (int i = 0; i < elements; i++) {
			if (i == 0) {
				screen.drawSprite(Sprite.WALL, 0, view.transformRect(wall));
			} else {
				screen.drawSprite(Sprite.WALL, 1, view.transformRect(wall));
			}
			
			wall[0][1] += wall[1][1];
		}
		
		if (position[0] + size[0] < view.position[0]) {
			removed = true;
		}
	}

	public float beginTop() {
		return Float.NaN;
	}

	public float topAt(List<Event> events, float[][] bounds) {
		if ((!passed) && (bounds[0][0] < position[0] + size[0] * 2 / 3) && (bounds[0][0] + bounds[1][0] >= position[0] + size[0] / 3)) {
			if (events != null) {
				if (bounds[0][1] + bounds[1][1] >= position[1]) {
					events.add(new WallEvent(this, true));
				} else {
					events.add(new WallEvent(this, false));
				}
			}
			
			return position[1];
		} else {
			return Float.NaN;
		}
	}

}
