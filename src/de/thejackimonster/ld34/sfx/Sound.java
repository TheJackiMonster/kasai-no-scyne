package de.thejackimonster.ld34.sfx;

import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public final class Sound {

	public static Sound RUN;

	private final AudioInputStream stream;

	private Sound(String name) throws UnsupportedAudioFileException, IOException {
		stream = AudioSystem.getAudioInputStream(Sound.class.getResource("/content/sfx/" + name + ".wav"));
	}

	public boolean play() {
		try {
			final Clip clip = AudioSystem.getClip();
			
			clip.open(stream);
			clip.setLoopPoints(0, -1);
			clip.loop(Clip.LOOP_CONTINUOUSLY);
			clip.start();
		} catch (LineUnavailableException e) {
			return false;
		} catch (IOException e) {
			return false;
		}
		
		return true;
	}

	static {
		try {
			RUN = new Sound("run");
		} catch (UnsupportedAudioFileException | IOException e) {
			e.printStackTrace();
			System.exit(2);
		}
	}

}
