package de.thejackimonster.ld34.gfx;

public final class View {

	public static final float SIZE = 16;

	public final float[] position;
	public final float[] size;

	private final float[] temp_rect;

	protected View(float width, float height) {
		position = new float[] { 0, 0 };
		size = new float[] { width, height };
		
		temp_rect = new float[4];
	}

	public final void lookAt(float[][] bounds) {
		position[0] = bounds[0][0] + (bounds[1][0] - size[0]) / 2;
		position[1] = bounds[0][1] + (bounds[1][1] - size[1]) / 2;
	}

	public final float[] transformRect(float[][] bounds) {
		temp_rect[0] = (bounds[0][0] - position[0]) / size[0];
		temp_rect[1] = (bounds[0][1] - position[1]) / size[1];
		temp_rect[2] = bounds[1][0] / size[0];
		temp_rect[3] = bounds[1][1] / size[1];
		
		return temp_rect;
	}

	public final float[] fillRect() {
		temp_rect[0] = 0;
		temp_rect[1] = 0;
		temp_rect[2] = 1;
		temp_rect[3] = 1;
		
		return temp_rect;
	}

}
