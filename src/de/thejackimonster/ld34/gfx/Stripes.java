package de.thejackimonster.ld34.gfx;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public final class Stripes {

	public static final int SIZE = Sprite.SIZE * 3;

	public static Stripes STREET;
	public static Stripes STREET_HOLE;

	private final BufferedImage image;

	private Stripes(String name) throws IOException {
		image = ImageIO.read(Sprite.class.getResource("/content/gfx/" + name + ".png"));
	}

	protected final void draw(Graphics gfx, int step, int x, int y, int width, int height, int maxY) {
		gfx.drawImage(image, x, y, x + width, y + height / 4, (step % SIZE), 0, (step % SIZE) + 1, image.getHeight() / 4, null);
		gfx.drawImage(image, x, y + height / 4, x + width, maxY, (step % SIZE), image.getHeight() / 4, (step % SIZE) + 1, image.getHeight(), null);
	}

	static {
		try {
			STREET = new Stripes("street");
			STREET_HOLE = new Stripes("street_hole");
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

}
