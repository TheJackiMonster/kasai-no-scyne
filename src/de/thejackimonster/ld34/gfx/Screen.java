package de.thejackimonster.ld34.gfx;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyListener;
import java.awt.image.BufferStrategy;

public final class Screen {

	private final Canvas canvas;

	private Graphics gfx;

	public Screen(Container container, KeyListener keyListener) {
		canvas = new Canvas();
		canvas.setSize(container.getWidth(), container.getHeight());
		canvas.setBackground(Color.BLACK);
		canvas.addKeyListener(keyListener);
		
		container.setLayout(new BorderLayout());
		container.add(canvas);
	}

	public final boolean render() {
		final BufferStrategy bs = canvas.getBufferStrategy();
		
		if (bs == null) {
			canvas.createBufferStrategy(3);
			return false;
		}
		
		if (gfx == null) {
			gfx = bs.getDrawGraphics();
			
			gfx.setFont(new Font(gfx.getFont().getFontName(), Font.BOLD, 16));
		}
		
		bs.show();
		
		gfx.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
		
		return true;
	}

	public final void drawRect(Color color, float[] rect) {
		gfx.setColor(color);
		gfx.fillRect(
			(int) (canvas.getWidth() * rect[0]),
			(int) (canvas.getHeight() * rect[1]),
			(int) (canvas.getWidth() * rect[2]),
			(int) (canvas.getHeight() * rect[3])
		);
	}

	public final void drawSprite(Sprite sprite, int action, float[] rect) {
		sprite.draw(gfx, action,
			(int) (canvas.getWidth() * rect[0]),
			(int) (canvas.getHeight() * rect[1]),
			(int) (canvas.getWidth() * rect[2]) + 1,
			(int) (canvas.getHeight() * rect[3]) + 1
		);
	}

	public final void drawStripes(Stripes stripes, int offset, float[] data, float ratio, float[] rect) {
		final int count = data.length;
		
		for (int i = 0; i < count; i++) {
			stripes.draw(gfx, (offset + i),
				(int) (canvas.getWidth() * (rect[0] + i * rect[2] / count)),
				(int) (canvas.getHeight() * (rect[1] + data[i] / ratio)),
				(int) (canvas.getWidth() * rect[2] / count) + 1,
				(int) (canvas.getHeight() * rect[3]) + 1,
			canvas.getHeight());
		}
	}

	public final void drawText(String string, float[] rect) {
		gfx.drawString(string,
			(int) (canvas.getWidth() * rect[0]),
			(int) (canvas.getHeight() * rect[1]) + gfx.getFont().getSize()
		);
	}

	public final View createView() {
		return new View(View.SIZE * canvas.getWidth() / canvas.getHeight(), View.SIZE);
	}

}
